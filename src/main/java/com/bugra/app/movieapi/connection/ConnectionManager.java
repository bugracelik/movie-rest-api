package com.bugra.app.movieapi.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static Connection connection;

    // 1 kere
    static {
        try {
            //bugra mysql : "root" "bugra"
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=Turkey", "root", "bugra"); // 1
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void setConnection(Connection connection) {
        ConnectionManager.connection = connection;
    }
}
