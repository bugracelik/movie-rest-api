package com.bugra.app.movieapi.controller;

import com.bugra.app.movieapi.model.Movie;
import com.bugra.app.movieapi.services.MovieService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MovieRestController {

    @GetMapping("get-all-movie-from-database")
    public ArrayList<Movie> getAllMovie() throws SQLException {
        MovieService movieService = new MovieService();
        return movieService.getMovies();
    }

    @GetMapping("movie-by-name/{name}/{directorRequest}")
    public List<Movie> getMovieByNameAndDirector(@PathVariable String name, @PathVariable String directorRequest) throws SQLException {
        MovieService movieService = new MovieService();
        return movieService.getMovieByNameAndDirector(name, directorRequest);
    }

    @GetMapping("movie-by-genres/{genresis}")
    public List<Movie> getMovieByGenre(@PathVariable String genresis) throws SQLException {
        //genresi action olan filmleri getirsin
        MovieService movieService = new MovieService();
        return movieService.getMovieByGenre(genresis);
    }

    @GetMapping("movie-by-id/{id}")
    public Movie getById(@PathVariable Integer id) throws SQLException {
        //id'si verilen filmleri dönen endpoint
        MovieService movieService = new MovieService();
        return movieService.getMovieById(id);
    }

    @GetMapping("movie-by-director/{director}")
    public ArrayList<Movie> getByDirector(@PathVariable String director) throws SQLException {
        //director'ü verilen filmleri dönen endpoint
        MovieService movieService = new MovieService();
        return movieService.getMovieByDirector(director);
    }
    @GetMapping("create-movie/{name}/{year}/{director}/{writers}/{stars}/{cast}/{genres}")
    public void createMovie(@PathVariable String name, @PathVariable Integer year, @PathVariable String director, @PathVariable String writers, @PathVariable String stars, @PathVariable String cast, @PathVariable String genres) throws SQLException {
        MovieService movieService = new MovieService();
        movieService.createMovie(name, year, director, writers, stars, cast, genres);
    }
    @GetMapping("update-movie-by-id/{id}/{name}/{year}")
    public void updateMovieById(@PathVariable Integer id, @PathVariable String name, @PathVariable Integer year) throws SQLException {
        MovieService movieService = new MovieService();
        movieService.updateMovieById(id, name, year);
    }
    @GetMapping("delete-movie-by-name/{name}")
    public String deleteMovieByName(@PathVariable String name) throws SQLException {
        MovieService movieService = new MovieService();
        boolean executeresult = movieService.deleteMovieByName(name);
        if (executeresult == false)
            return "başarılı bir şekilde silindi";
        else
            return "filminiz silinemedi";
    }

}
