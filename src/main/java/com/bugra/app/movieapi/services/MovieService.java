package com.bugra.app.movieapi.services;


import com.bugra.app.movieapi.model.Movie;
import com.bugra.app.movieapi.repository.MovieRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MovieService {

    public Movie getMovieById(Integer id) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.getMovieById(id);
    }

    public ArrayList<Movie> getMovies() throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.getAllMoviesFromDatabase();
    }

    public List<Movie> getMovieByNameAndDirector(String name, String directorRequest) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.getMovieByNameAndDirector(name, directorRequest);
    }

    public List<Movie> getMovieByGenre(String genresis) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.getMoviesByGenre(genresis);
    }

    public ArrayList<Movie> getMovieByDirector(String director) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.getMovieByDirector(director);
    }

    public void createMovie(String name, Integer year, String director, String writers, String stars, String cast, String genres) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        movieRepository.createMovie(name, year, director, writers, stars, cast, genres);
    }

    public void updateMovieById(Integer id, String name, Integer year) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        movieRepository.updateMovieById(id, name, year);
    }

    public boolean deleteMovieByName(String name) throws SQLException {
        MovieRepository movieRepository = new MovieRepository();
        return movieRepository.deleteMovieByName(name);

    }
}
