package com.bugra.app.movieapi.model;

public class MovieBuilder {

    private Integer movie_id;
    private String name;
    private Integer year;
    private String director;
    private String writers;
    private String starts;
    private String cast;
    private String genres;

    public MovieBuilder withId(Integer movie_id) {
        this.movie_id = movie_id;
        return this;
    }

    public MovieBuilder withName(String name) {
        this.name = name;
        return this; // fluent
    }

    public MovieBuilder withYear(Integer year) {
        this.year = year;
        return this;
    }

    public MovieBuilder withDirector(String director) {
        this.director = director;
        return this;
    }

    public MovieBuilder withWriter(String writers) {
        this.writers = writers;
        return this;
    }

    public MovieBuilder withStars(String stars) {
        this.starts = stars;
        return this;
    }

    public MovieBuilder withCast(String cast) {
        this.cast = cast;
        return this;
    }

    public MovieBuilder withGenres(String genres) {
        this.genres = genres;
        return this;
    }

    public Movie build() {
        return new Movie(this.movie_id, this.name, this.year, this.director, this.writers, this.starts, this.cast, this.genres);
    }
}
