package com.bugra.app.movieapi.model;

public class Movie {
    private Integer id;
    private String name;
    private Integer year;
    private String director;
    private String writers;
    private String stars;
    private String cast;
    private String genres;

    public Movie(Integer id, String name, Integer year, String director, String writers, String stars, String cast, String genres) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.director = director;
        this.writers = writers;
        this.stars = stars;
        this.cast = cast;
        this.genres = genres;
    }

    public Movie(){

    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", director='" + director + '\'' +
                ", writers='" + writers + '\'' +
                ", stars='" + stars + '\'' +
                ", cast='" + cast + '\'' +
                ", genres='" + genres + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }
}
