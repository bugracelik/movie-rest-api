package com.bugra.app.movieapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class MovieApiApplication {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(MovieApiApplication.class, args);
    }
}
//deneme