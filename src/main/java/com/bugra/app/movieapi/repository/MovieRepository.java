package com.bugra.app.movieapi.repository;
import com.bugra.app.movieapi.connection.ConnectionManager;
import com.bugra.app.movieapi.model.MovieBuilder;
import com.bugra.app.movieapi.model.Movie;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MovieRepository {

    public ArrayList<Movie> getAllMoviesFromDatabase() throws SQLException {

        String name;
        Integer year;
        Integer id;
        String director;
        String writers;
        String stars;
        String cast;
        String genres;

        String sql = "SELECT t.*\n" +
                "            FROM bugra.moviedb t\n" +
                "            LIMIT 501";

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<Movie> movies = new ArrayList<>();

        //git
        while (resultSet.next()) {
            id = resultSet.getInt("id");
            name = resultSet.getString("name");
            year = resultSet.getInt("year");
            director = resultSet.getString("director");
            writers = resultSet.getString("writers");
            stars = resultSet.getString("stars");
            cast = resultSet.getString("cast");
            genres = resultSet.getString("genres");
            Movie movie = new Movie(id, name, year, director, writers, stars, cast, genres);
            movies.add(movie);
        }

        for (Movie movie : movies) {
            System.out.println(movie);
        }

        //ArrayList dönüyorum
        return movies;
    }

    public List<Movie> getMovieByNameAndDirector(String name, String directorRequest) throws SQLException {
        // ismi mandıra filozofu filmi getir.
        String movie_name;
        Integer year;
        Integer id;
        String director;
        String writers;
        String stars;
        String cast;
        String genres;

        ArrayList<Movie> movies = new ArrayList<>();
        String formattedStringSql = String.format("SELECT * from bugra.moviedb where name = '%s' and director= '%s'", name, directorRequest);
        PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(formattedStringSql);
        ResultSet resultSet = preparedStatement.executeQuery(); // 10000000

        while (resultSet.next()) {
            movie_name = resultSet.getString("name");
            id = resultSet.getInt("id");
            year = resultSet.getInt("year");
            director = resultSet.getString("director");
            writers = resultSet.getString("writers");
            stars = resultSet.getString("stars");
            cast = resultSet.getString("cast");
            genres = resultSet.getString("genres");
            Movie movie = new Movie(id, movie_name, year, director, writers, stars, cast, genres);
            movies.add(movie);
        }

        return movies;
    }

    public List<Movie> getMoviesByGenre(String genresis) throws SQLException {
        String name;
        Integer year;
        Integer id;
        String director;
        String writers;
        String stars;
        String cast;
        String genres;

        ArrayList<Movie> movies = new ArrayList<>();
        //sqlword kendim oluşturdum, genresis dışarıdan parametle ile geliyor.
        String sqlword = "SELECT * FROM bugra.moviedb Where genres LIKE " + "'%" + genresis + "%'";
        //format da %s oldugu için hata veriyor %%s% olmuyor. O yüzden kendim sqlword yazdım
        String format = String.format("SELECT * from bugra.moviedb where genres LIKE '%s'", genresis);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlword);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            id = resultSet.getInt("id");
            name = resultSet.getString("name");
            year = resultSet.getInt("year");
            director = resultSet.getString("director");
            writers = resultSet.getString("writers");
            stars = resultSet.getString("stars");
            cast = resultSet.getString("cast");
            genres = resultSet.getString("genres");
            Movie movie = new Movie(id, name, year, director, writers, stars, cast, genres);
            movies.add(movie);
        }

        return movies;
    }

    public Movie getMovieById(Integer id) throws SQLException {
        String name;
        Integer year;
        Integer movie_id;
        String director;
        String writers;
        String stars;
        String cast;
        String genres;

        String formatSql = String.format("SELECT * FROM bugra.moviedb where id = '%d' ", id);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(formatSql);
        ResultSet resultSet = preparedStatement.executeQuery();

        resultSet.next(); // header geçtim
        name = resultSet.getString("name");
        year = resultSet.getInt("year");
        movie_id = resultSet.getInt("id");
        director = resultSet.getString("director");
        writers = resultSet.getString("writers");
        stars = resultSet.getString("stars");
        cast = resultSet.getString("cast");
        genres = resultSet.getString("genres");
        // kod konuşuyor
        Movie movie = new MovieBuilder().withId(movie_id).withName(name).withYear(year).withDirector(director).withWriter(writers).withStars(stars).withCast(cast).withGenres(genres).build();
        return movie;
    }

    public ArrayList<Movie> getMovieByDirector(String director) throws SQLException {

        String name;
        Integer year;
        Integer movie_id;
        String movie_director;
        String writers;
        String stars;
        String cast;
        String genres;

        ArrayList<Movie> movies = new ArrayList<>();
        String formatSql = String.format("SELECT * FROM bugra.moviedb where director LIKE '%%%s%%' ", director);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(formatSql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            name = resultSet.getString("name");
            year = resultSet.getInt("year");
            movie_id = resultSet.getInt("id");
            movie_director = resultSet.getString("director");
            writers = resultSet.getString("writers");
            stars = resultSet.getString("stars");
            cast = resultSet.getString("cast");
            genres = resultSet.getString("genres");
            Movie movie = new Movie(movie_id, name, year, movie_director, writers, stars, cast, genres);
            movies.add(movie);
        }
        return movies;
    }

    public void createMovie(String name, Integer year, String director, String writers, String stars, String cast, String genres) throws SQLException {
        String formatsql = String.format("INSERT INTO bugra.moviedb (name, year, director, cast, stars, genres, writers) VALUES ('%s', %d, '%s', '%s', '%s', '%s', '%s')",name, year, director, writers, stars, cast, genres);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(formatsql);
        boolean execute = preparedStatement.execute();

    }

    public void updateMovieById(Integer id, String name, Integer year) throws SQLException {
        String format = String.format("UPDATE bugra.moviedb SET name = '%s', year = %d WHERE id = %d", name, year, id);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(format);
        boolean execute = preparedStatement.execute();
    }

    public Boolean deleteMovieByName(String name) throws SQLException {
        String format = String.format("DELETE FROM bugra.moviedb WHERE name='%s'", name);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(format);
        return preparedStatement.execute();
    }
}
